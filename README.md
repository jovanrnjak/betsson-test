# Betsson Group: Movies Application

## Running the application
The application skeleton was generated with the Angular CLI so to run the application `npm install` followed by `ng serve` is all that should be necessary. All the other `ng` commands such as `ng test` should also be working as intended.

## Using the application
The application has two screens. The home screen should display a list of movies and contain a search box and filter bar. Upon input the search bar will narrow down the movies shown to contain only those where the title includes the search term. The filter bar begins in a state where all filters are active with all the movies shown. Upon clicking a genre it will narrow down the selection to only movies of that genre, enabling additional filters will then add onto the selection of allowed genres. If all filters are disabled save one toggling the enabled filter will make all the filters active once more. Filter state is persisted on reload; while this may not be the best from a user-perspective I decided to have a bit of fun and see how nicely redux would play with angular in that regard so I left that feature in.

Once a movie is clicked the application transitions to the second screen with details about the movie as well as a list of suggested movies that when clicked will display information about a newly selected movie. The only other interactable element on this screen is the back arrow in the upper right which when clicked will transition the application back to the home screen. The phone/browser back/forward buttons can be used in a similar way, the only difference being that if another movie was clicked on in the details page the back button will navigate to the previous movie details and not the home screen.

There are two responsive layouts available, one that caters to desktop, tablet and some phones in landscape mode while the other is for the standard portrait mode phone view. There are a few differences in layout/animations so I do suggest taking a look at both screens in both layout variants.

Also, I have placed a few animations for flair while hopefully not overdoing it. Namely transitions between screens, hovering over movies, a loading indicator and a suggested movies popout.

## About the app

Angular 6, Typescript, Rxjs and Sass were  used as required. I believe the only additional dependency ontop of the default Angular CLI generated ones would be `@angular-redux/store` for the Redux implementation. 

I've had quite some difficulty hooking up `@angular-redux/store` to Angular's unit testing and considering all the state is in the store it has been a bit challenging to compose even some basic unit tests. I was considering opting for `ngrx` for the redux-like implementation since it has a lot more documentation (especially in regards to testing), but decided against it since `@angular-redux/store` seemed like it would fit the task requirements better.

## Closing
In closing I would like to admit that I'm still learning Angular and that it has been rather enjoyable. When it comes to React I would say that I am quite comfortable but in Angular even seemingly simple problems, like when component separation is justified, have given me food for thought; I've still yet to fully grasp what "production-ready" code should look like in Angular. This test however has been a great introduction to it, I would gladly go over the challenges, decisions and rewrites I came across while writing the app, if such a thing is desirable of course.

