import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { SearchActions, FilterActions } from '../app.actions';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux } from '@angular-redux/store/testing';
import { rootReducer, INITIAL_STATE } from '../store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoviesComponent } from './movies.component';
import { MovieComponent } from '../movie/movie.component';
import { MoviesFiltersComponent } from '../movies-filters/movies-filters.component';
import { By } from '@angular/platform-browser';
import { MOVIES } from '../mock-movies';

describe('MoviesComponent', () => {
  let component: MoviesComponent;
  let fixture: ComponentFixture<MoviesComponent>;
  const mockSearchActions = new SearchActions();
  const mockFilterActions = new FilterActions();
  const mockNgRedux: NgRedux<any> = MockNgRedux.getInstance();
  mockNgRedux.configureStore(
    rootReducer,
    {
      ...INITIAL_STATE,
      moviesFiltered: MOVIES[0]
    }
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesComponent, MovieComponent, MoviesFiltersComponent ],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: SearchActions, useValue: mockSearchActions},
        { provide: FilterActions, useValue: mockFilterActions},
        { provide: NgRedux, useValue: mockNgRedux }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('search input should call onKey', () => {
    spyOn(component, 'onKey');
    const searchInput = fixture.debugElement.query(By.css('input'));
    searchInput.nativeElement.value = 'why is this here';
    searchInput.triggerEventHandler('keyup', {});
    expect(component.onKey).toHaveBeenCalled();
  });
});
