import { Component, OnInit, HostBinding, OnDestroy } from '@angular/core';
import { Movie } from '../movie';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { fadeInOutAnimation } from '../animations';
import { SearchActions } from '../app.actions';
import { IAppState } from '../store';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  animations: [fadeInOutAnimation],
  styleUrls: ['./movies.component.sass']
})
export class MoviesComponent implements OnInit, OnDestroy {

  @HostBinding('@routeAnimation') routeAnimation = true;
  @select('moviesFiltered') moviesFiltered$: Observable<Movie[]>;

  constructor(
    private actions: SearchActions,
    private ngRedux: NgRedux<IAppState>
  ) { }

  onKey(value: string): void {
    this.ngRedux.dispatch(this.actions.changeSearch(value));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.ngRedux.dispatch(this.actions.changeSearch(''));
  }
}
