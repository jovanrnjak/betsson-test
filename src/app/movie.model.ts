export enum GenreType {
    Action = 'action',
    Adventure = 'adventure',
    Biography = 'biography',
    Comedy = 'comedy',
    Crime = 'crime',
    Drama = 'drama',
    History = 'history',
    Mystery = 'mystery',
    Scifi = 'scifi',
    Sport = 'sport',
    Thriller = 'thriller'
}

const filters = {};
const filtersOff = {};
Object.keys(GenreType).forEach(genre => {
  filters[GenreType[genre]] = true;
  filtersOff[GenreType[genre]] = false;
});

export const FiltersNum = Object.keys(filters).length;
export const FilterList = filters;
export const FilterListOff = filtersOff;
