// Choose N random elements from an array, no repeats
export function takeN<S> (n: number, values: S[]): S[] {
  const selection: S[] = [];
  const taken: object = {};
  while (n) {
    const id = Math.floor(values.length * Math.random());
    if (taken[id] === undefined) {
      taken[id] = true;
      selection.push(values[id]);
      n--;
    }
  }
  return selection;
}
