import { FilterActions, MovieActions, SearchActions } from './app.actions';
import { Movie } from './movie';
import { rootReducer, INITIAL_STATE, IAppState } from './store';
import { MOVIES } from './mock-movies';
import { FiltersNum } from './movie.model';

// Pure function testing

describe('reduxStore', () => {
  let store: IAppState;

  beforeEach(() => {
    store = INITIAL_STATE;
  });

  it('should add movies', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    expect(<Movie[]>store.movies !== undefined).toBeTruthy();
  });

  it('should enable only toggled filter if all filters are enabled', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    const result =  Object.keys(store.filters).every((genre) => {
      return ((genre !== 'comedy' && store.filters[genre] === false) || (store.filters[genre] === true && genre === 'comedy'));
    }) && store.enabledFilters === 1;
    expect(result).toBeTruthy();
  });

  it('should enable all filters if only toggled filter is enabled', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'drama'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'drama'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    const result =  Object.keys(store.filters).every((genre) => {
      return (store.filters[genre] === true);
    }) && store.enabledFilters === FiltersNum;
    expect(result).toBeTruthy();
  });

  it('should filter by genre', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'drama'});
    const result = store.moviesFiltered.every((movie) => {
      return (movie.genres.some((genre) => {
        return(genre === 'comedy' || genre === 'drama');
      }));
    });
    expect(result).toBeTruthy();
  });

  it('should filter by search', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: SearchActions.CHANGE, payload: 'the'});
    const result = store.moviesFiltered.every((movie) => {
      return (movie.name.toLowerCase().indexOf('the') >= 0);
    });
    expect(result).toBeTruthy();
  });

  it('should filter by search then genre', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: SearchActions.CHANGE, payload: 'the'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'drama'});
    const result = store.moviesFiltered.every((movie) => {
      return (
        (movie.name.toLowerCase().indexOf('the') >= 0)
        && (movie.genres.some((genre) => {
          return(genre === 'comedy' || genre === 'drama');
        }))
      );
    });
    expect(result).toBeTruthy();
  });

  it('should filter by genre then search', () => {
    store = rootReducer(store, { type: MovieActions.ADD, payload: MOVIES });
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'comedy'});
    store = rootReducer(store, { type: FilterActions.TOGGLE, payload: 'drama'});
    store = rootReducer(store, { type: SearchActions.CHANGE, payload: 'the'});
    const result = store.moviesFiltered.every((movie) => {
      return (
        (movie.name.toLowerCase().indexOf('the') >= 0)
        && (movie.genres.some((genre) => {
          return(genre === 'comedy' || genre === 'drama');
        }))
      );
    });
    expect(result).toBeTruthy();
  });
});
