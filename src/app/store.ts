import { AnyAction } from 'redux';
import { FilterActions, MovieActions, SearchActions } from './app.actions';
import { FilterList, FilterListOff, FiltersNum } from './movie.model';
import { Movie } from './movie';

export interface IAppState {
  filters: object;
  // Since filters is a dictionary keep track of
  // enabled filters to avoid recalculating every time
  enabledFilters: number;
  movies: Movie[];
  moviesFiltered: Movie[];
  searchTerm: string;
}

export const INITIAL_STATE: IAppState = {
  filters: FilterList,
  enabledFilters: FiltersNum,
  movies: [],
  moviesFiltered: [],
  searchTerm: ''
};

// Alternative initial state for persisted properties
export const LOADED_STATE: IAppState =
    (localStorage.getItem('store') !== null)
      ? {...INITIAL_STATE, ...JSON.parse(localStorage.getItem('store'))}
      : INITIAL_STATE;

const filteredMovies = (movies: Movie[], filters: object) => {
    const filtered = movies.filter((movie) => {
      return (
        movie.genres.find((genre) => {
          return(filters[genre] === true);
        })
      );
    });
    return filtered;
};

const searchedMovies = (movies: Movie[], term: string) => {
  if (term !== '') {
    return(
      movies.filter(movie => {
        return (movie.name.toLowerCase().indexOf(term) >= 0);
      })
    );
  } else {
    return movies;
  }
};

export function rootReducer(lastState: IAppState, action: AnyAction): IAppState {
  switch (action.type) {
    case FilterActions.TOGGLE: {
      let enabledFilters: number;
      let filters: object;
      if (lastState.enabledFilters === FiltersNum) {
        enabledFilters = 1;
        filters = { ...FilterListOff, [action.payload]: true};
      } else if (lastState.enabledFilters === 1 && lastState.filters[action.payload] === true) {
        enabledFilters = FiltersNum;
        filters = FilterList;
      } else {
        enabledFilters = (lastState.enabledFilters + ((lastState.filters[action.payload] === true) ? -1 : 1));
        filters = {
          ...lastState.filters,
          [action.payload] : lastState.filters[action.payload] === true ? false : true
        };
      }
      lastState = {
        ...lastState,
        filters,
        enabledFilters,
        moviesFiltered: filteredMovies(searchedMovies(lastState.movies, lastState.searchTerm), filters)
      };
      break;
    }
    case MovieActions.ADD: {
      lastState = {
        ...lastState,
        movies: action.payload,
        moviesFiltered: filteredMovies(searchedMovies(action.payload, lastState.searchTerm), lastState.filters)
      };
      break;
    }
    case SearchActions.CHANGE: {
      lastState = {
        ...lastState,
        searchTerm: action.payload,
        moviesFiltered: filteredMovies(searchedMovies(lastState.movies, action.payload), lastState.filters)
      };
      break;
    }

  }

  // Save filter options for later visits
  localStorage.setItem('store', JSON.stringify({
    filters: lastState.filters,
    enabledFilters: lastState.enabledFilters,
  }));

  return lastState;
}
