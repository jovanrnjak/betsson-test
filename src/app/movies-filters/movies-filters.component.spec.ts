import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FilterActions } from '../app.actions';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux } from '@angular-redux/store/testing';
import { rootReducer, INITIAL_STATE } from '../store';
import { MoviesFiltersComponent } from './movies-filters.component';
import { By } from '@angular/platform-browser';

describe('MoviesFiltersComponent', () => {
  let component: MoviesFiltersComponent;
  let fixture: ComponentFixture<MoviesFiltersComponent>;
  const mockFilterActions = new FilterActions();
  const mockNgRedux: NgRedux<any> = MockNgRedux.getInstance();
  mockNgRedux.configureStore(
    rootReducer,
    INITIAL_STATE
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesFiltersComponent ],
      providers: [
        { provide: FilterActions, useValue: mockFilterActions},
        { provide: NgRedux, useValue: mockNgRedux }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesFiltersComponent);
    component = fixture.componentInstance;
    component.filtersList = [{
      title: 'comedy', enabled: true
    }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain filter class', () => {
    const filter = fixture.debugElement.query(By.css('.filter'));
    expect(filter).toBeTruthy();
  });

  it('filter class should call toggleFilter', () => {
    spyOn(component, 'toggleFilter');
    const filterButton = fixture.debugElement.query(By.css('.filter'));
    filterButton.nativeElement.click();
    expect(component.toggleFilter).toHaveBeenCalled();
  });
});
