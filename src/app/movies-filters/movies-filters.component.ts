import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { FilterActions } from '../app.actions';
import { IAppState } from '../store';
import { Observable, Subscription } from 'rxjs';

interface Filter {
  title: string;
  enabled: boolean;
}

@Component({
  selector: 'app-movies-filters',
  templateUrl: './movies-filters.component.html',
  styleUrls: ['./movies-filters.component.sass']
})
export class MoviesFiltersComponent implements OnInit, OnDestroy {

  @select('filters') readonly filters$: Observable<Object>;
  filtersList: Filter[];
  filtersSubscription: Subscription;

  constructor(
    private actions: FilterActions,
    private ngRedux: NgRedux<IAppState>
  ) { }

  ngOnInit() {
    this.filtersSubscription = this.filters$.subscribe(filters => {
      this.filtersList = Object.keys(filters).map(genre => {
        return({ title: genre, enabled: filters[genre] === true ? true : false});
      });
    });
  }

  ngOnDestroy() {
    this.filtersSubscription.unsubscribe();
  }

  toggleFilter(genre: string) {
    this.ngRedux.dispatch(this.actions.toggleFilter(genre));
  }
}
