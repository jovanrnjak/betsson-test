import { Injectable } from '@angular/core';
import { Movie } from './movie';
import { MOVIES } from './mock-movies';
import { Observable, of } from 'rxjs';
import { MovieActions } from './app.actions';
import { NgRedux } from '../../node_modules/@angular-redux/store';
import { IAppState } from './store';
import { takeN } from './app.helpers';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(
    private actions: MovieActions,
    private ngRedux: NgRedux<IAppState>
  ) { }

  fetchMovies(cb?: () => void): void {
    // using setTimeout to fake waiting on server, set delay !== 0 for effect
    setTimeout(() => {
      this.ngRedux.dispatch(this.actions.addMovies(MOVIES));
      if (cb) {
        cb();
      }
    }, 800);
  }

  getMovie(id: number): Observable<Movie> {
    return of(MOVIES.find(movie => movie.id === id));
  }

  getSuggestions(id: number): Observable<Movie[]> {
    return new Observable((observer) => {
      // using setTimeout to fake waiting on server, set delay !== 0 for effect
      setTimeout(() => {
        const genres = MOVIES[id].genres;
        const suggestions = MOVIES.filter((movie) => {
          return (movie.id !== id && movie.genres.some((movieGenre) => {
            return genres.some((genre) => {
              return (genre === movieGenre);
            });
          }));
        });
        observer.next(takeN(4, suggestions));
      }, 100);
    });
  }
}
