import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieDetailComponent } from './movie-detail.component';
import { MovieComponent } from '../movie/movie.component';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { rootReducer, INITIAL_STATE } from '../store';
import { MovieActions } from '../app.actions';
import { NgRedux } from '../../../node_modules/@angular-redux/store';
import { MockNgRedux } from '../../../node_modules/@angular-redux/store/lib/testing';

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;
  const mockMovieActions = new MovieActions();
  const mockNgRedux: NgRedux<any> = MockNgRedux.getInstance();
    mockNgRedux.configureStore(
      rootReducer,
      INITIAL_STATE
    );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieDetailComponent, MovieComponent ],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: { params: of({ id: '1' }) } },
        { provide: MovieActions, useValue: mockMovieActions },
        { provide: NgRedux, useValue: mockNgRedux }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
