import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { slideInOutAnimation } from '../animations';
import { Subscription, Observable } from 'rxjs';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  animations: [slideInOutAnimation],
  styleUrls: ['./movie-detail.component.sass']
})
export class MovieDetailComponent implements OnInit, OnDestroy {

  @HostBinding('@routeAnimation') routeAnimation = true;

  movie: Movie;
  suggestions: Movie[];
  suggestions$: Observable<Movie[]>;
  movieSubscription: Subscription;
  movie$: Observable<Movie>;
  movieSuggestionsSubscription: Subscription;
  routeSubscription: Subscription;
  paramId: number;

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService
  ) {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.paramId = +params.id;
      this.movie$ = this.movieService.getMovie(this.paramId);
      this.movieSuggestionsSubscription = this.movieService.getSuggestions(this.paramId).subscribe(movies => {
        this.suggestions = movies;
      });
    });
   }

  ngOnDestroy() {
    if (this.movieSuggestionsSubscription) {
      this.movieSuggestionsSubscription.unsubscribe();
    }
  }

  ngOnInit() {
  }

}
