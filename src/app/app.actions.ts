import { Injectable } from '@angular/core';
import { AnyAction } from 'redux';
import { Movie } from './movie';

@Injectable()
export class FilterActions {
  static TOGGLE = 'FILTER_TOGGLE';
  toggleFilter(value: string): AnyAction {
    return {
      type:  FilterActions.TOGGLE,
      payload:  value.toLowerCase()
    };
  }
}

@Injectable()
export class SearchActions {
  static CHANGE = 'SEARCH_CHANGE';
  changeSearch(value: string): AnyAction {
    return {
      type: SearchActions.CHANGE,
      payload: value.toLowerCase()
    };
  }
}

@Injectable()
export class MovieActions {
  static ADD = 'MOVIE_ADD';
  addMovies(movies: Movie[]): AnyAction {
    return {
      type: MovieActions.ADD,
      payload: movies
    };
  }
}
