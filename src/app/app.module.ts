import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { createLogger } from 'redux-logger';
import { rootReducer, IAppState, INITIAL_STATE, LOADED_STATE } from './store';
import { FilterActions, SearchActions, MovieActions } from './app.actions';

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { AppRoutingModule } from './/app-routing.module';
import { MovieComponent } from './movie/movie.component';
import { MoviesFiltersComponent } from './movies-filters/movies-filters.component';
import { MovieService } from './movie.service';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieDetailComponent,
    MovieComponent,
    MoviesFiltersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    NgReduxModule
  ],
  providers: [FilterActions, SearchActions, MovieActions],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(
    ngRedux: NgRedux<IAppState>,
    movieService: MovieService
  ) {
    ngRedux.configureStore(
      rootReducer,
      LOADED_STATE,
      // Uncomment for redux logging
      // [ createLogger() ]
    );

    movieService.fetchMovies();
  }
}
