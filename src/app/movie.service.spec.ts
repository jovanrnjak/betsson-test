import { TestBed, inject } from '@angular/core/testing';
import { Movie } from './movie';
import { MovieService } from './movie.service';
import { rootReducer, INITIAL_STATE } from './store';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux } from '@angular-redux/store/testing';
import { MovieActions } from './app.actions';

describe('MovieService', () => {

  let service: MovieService;
  let mockNgRedux: NgRedux<any>;
  beforeEach(() => {
    const mockMovieActions = new MovieActions();
    mockNgRedux = MockNgRedux.getInstance();
    mockNgRedux.configureStore(
      rootReducer,
      INITIAL_STATE
    );

    service = new MovieService(mockMovieActions, mockNgRedux);
    TestBed.configureTestingModule({
      providers: [
        MovieService,
        { provide: MovieActions, useValue: mockMovieActions },
        { provide: NgRedux, useValue: mockNgRedux }
      ]
    });
  });

  it('should create', inject([MovieService], (mservice: MovieService) => {
    expect(mservice).toBeTruthy();
  }));

  it('getSuggestions should return Movie array from observable',
    (done: DoneFn) => {
    service.getSuggestions(1).subscribe(value => {
      expect(<Movie[]>value !== undefined).toBeTruthy();
      done();
    });
  });

  it('fetchMovies should update redux store with movies',
    (done: DoneFn) => {
    service.fetchMovies(() => {
      // check redux store
      done();
    });
  });

  it('getMovie should return correct value from observable',
    (done: DoneFn) => {
    service.getMovie(1).subscribe(value => {
      expect(value.key).toBe('deadpool');
      done();
    });
  });

});
