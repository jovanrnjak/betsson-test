import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

export const slideInOutAnimation = trigger('routeAnimation', [
  state(
    '*',
    style({
      opacity: 1,
      transform: 'translateX(0)'
    })
  ),
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'translateX(-100%)'
    }),
    animate('0.35s ease-out')
  ]),
  transition(':leave', [
    animate(
      '0.35s ease-out',
      style({
        opacity: 0,
        transform: 'translateX(100%)'
      })
    )
  ])
]);

export const fadeInOutAnimation = trigger('routeAnimation', [
  state(
    '*',
    style({
      opacity: 1
    })
  ),
  transition(':enter', [
    style({
      opacity: 0
    }),
    animate('0.35s ease-in')
  ]),
  transition(':leave', [
    animate(
      '0.35s ease-out',
      style({
        opacity: 0
      })
    )
  ])
]);
